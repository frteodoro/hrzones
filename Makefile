build/hrzones: HRZones.hs build
		ghc -main-is HRZones -o $@ -outputdir $(@D) $<

build:
	mkdir $@

.PHONY: clean

clean:
	rm -rf build
