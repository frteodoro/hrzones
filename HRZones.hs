#!/usr/bin/env runhaskell

module HRZones 
    ( hrZones
    , main
    ) where

import System.Environment (getArgs)

-- HR: Heart Rate
type HR = Integer
type HRZone = (HR, HR)
type HRZones = (HRZone, HRZone, HRZone, HRZone, HRZone)

hrZones :: HR -> HR -> HRZones
hrZones restingHR maxHR = (z1, z2, z3, z4, z5)
    where reserve = maxHR - restingHR
          z1 = (restingHR, reservePercentHR 0.6)
          z2 = (snd z1, reservePercentHR 0.7)
          z3 = (snd z2, reservePercentHR 0.8)
          z4 = (snd z3, reservePercentHR 0.9)
          z5 = (snd z4, maxHR)
          reservePercentHR percent = round $ (fromInteger restingHR) + (fromInteger reserve) * percent

main = do 
    [restingHR, maxHR] <- getArgs
    putStrLn $ show $ hrZones (read restingHR) (read maxHR)
